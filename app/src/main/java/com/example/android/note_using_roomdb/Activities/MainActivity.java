package com.example.android.note_using_roomdb.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.android.note_using_roomdb.Model.Note;
import com.example.android.note_using_roomdb.Adapter.NoteAdapter;
import com.example.android.note_using_roomdb.ViewModel.NoteViewModel;
import com.example.android.note_using_roomdb.R;

import java.util.List;

public class MainActivity extends AppCompatActivity implements NoteAdapter.OnNoteItemClick {

    FloatingActionButton btnAdd;
    RecyclerView mNoteRecyclerList;
    NoteAdapter noteAdapter;
    private List<Note> notes;
    int pos;
    private TextView textViewMsg;
    private NoteViewModel noteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewMsg=findViewById(R.id.nullText);
        mNoteRecyclerList=findViewById(R.id.noteRecyclerList);
        mNoteRecyclerList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        noteViewModel=ViewModelProviders.of(this).get(NoteViewModel.class);

        subscribeViewModel();
        noteAdapter=new NoteAdapter(notes,MainActivity.this);
        mNoteRecyclerList.setAdapter(noteAdapter);
        btnAdd=findViewById(R.id.addBtn);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,AddNoteActivity.class));
            }
        });
    }
    public void subscribeViewModel(){
        noteViewModel.getNoteLiveData().observe(MainActivity.this, new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                MainActivity.this.notes=notes;
                noteAdapter.setChange(notes);

            }
        });
    }
    @Override
    public void onNoteClick( final int pos) {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Select Options")
                .setItems(new String[]{"Delete", "Update"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i){
                            case 0:
                                Log.e("MainActivity",String.valueOf(pos));
                                noteViewModel.deleteData(notes.get(pos));
                                listVisibility();
                                break;
                            case 1:
                                MainActivity.this.pos = pos;
                                startActivityForResult(
                                        new Intent(MainActivity.this,
                                                AddNoteActivity.class).putExtra("note", notes
                                                .get
                                                        (pos)),
                                        100);
                                break;
                        }
                    }
                }).show();
    }
    private void listVisibility(){
        int emptyMsgVisibility = View.GONE;
        if (notes.size() == 0){
            if (textViewMsg.getVisibility() == View.GONE)
                emptyMsgVisibility = View.VISIBLE;
        }
        textViewMsg.setVisibility(emptyMsgVisibility);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unSubScribeNoteLiveData();
    }
    private void unSubScribeNoteLiveData() {
    if(noteViewModel!=null && noteViewModel.getNoteLiveData().hasActiveObservers()){
        noteViewModel.getNoteLiveData().removeObserver((Observer<List<Note>>) MainActivity.this);
    }
    }
}

