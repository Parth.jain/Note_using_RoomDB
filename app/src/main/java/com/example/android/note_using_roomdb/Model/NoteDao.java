package com.example.android.note_using_roomdb.Model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.android.note_using_roomdb.Constants;

import java.util.List;

/**
 * Created by parth on 6/3/18.
 */
@Dao
public interface NoteDao {

    @Query("SELECT * FROM "+ Constants.TABLE_NAME_NOTE+" order by note_id desc")
    List<Note> getNotes();

    @Insert
    long insertNote(Note note);

    @Update
    void updateNote(Note note);

    @Delete
    void deleteNote(Note note);

    @Query("Update "+Constants.TABLE_NAME_NOTE+" SET note_content=:desc, title=:title where " +
            "note_id=:id")
    void update_specific(int id, String title, String desc);

    @Query("Select * from "+Constants.TABLE_NAME_NOTE)
    LiveData<List<Note>> getAllNoteLive();

}






















 /* @Delete
    void deleteNotes(Note... notes);*/
