package com.example.android.note_using_roomdb.Activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.note_using_roomdb.Model.Note;
import com.example.android.note_using_roomdb.ViewModel.NoteViewModel;
import com.example.android.note_using_roomdb.R;

public class AddNoteActivity extends AppCompatActivity {

    private Button saveBtn;
    private EditText mEditTitle,mEditDescription;
    private Note note;
    private boolean update;
    private NoteViewModel mNoteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        saveBtn = findViewById(R.id.btnSave);
        mEditTitle = findViewById(R.id.editTitle);
        mEditDescription = findViewById(R.id.editDescription);
        mNoteViewModel=ViewModelProviders.of(this).get(NoteViewModel.class);

        if ((note = (Note) getIntent().getSerializableExtra("note")) != null) {
            getSupportActionBar().setTitle("Update Note");
            update = true;
            saveBtn.setText("Update");
            mEditTitle.setText(note.getTitle());
            mEditDescription.setText(note.getContent());
        }
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title=mEditTitle.getText().toString();
                String Description=mEditDescription.getText().toString();

                if (update) {
                    if(checkforNull(title,Description)) {
                    note.setTitle(title);
                    note.setContent(Description);
                        mNoteViewModel.updte_specific(note.getNote_id(),title,Description);
                    startActivity(new Intent(AddNoteActivity.this, MainActivity.class).setFlags
                            (Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                    }
                } else {
                    if(checkforNull(title,Description)){
                    note = new Note(Description,title);
                    mNoteViewModel.insertNote(note);
                    startActivity(new Intent(AddNoteActivity.this, MainActivity.class).setFlags
                            (Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                    }
                }
            }
        });
    }
    public boolean checkforNull(String title,String Description){
        if(!title.equals("") && !Description.equals("")){
            return true;
        }
        else{
            Toast.makeText(this, "Can Not Add Blank Note", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}


