package com.example.android.note_using_roomdb.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.android.note_using_roomdb.Model.Note;
import com.example.android.note_using_roomdb.Database.NoteDatabase;

import java.util.List;

/**
 * Created by parth on 8/3/18.
 */

public class NoteViewModel extends AndroidViewModel {

    public LiveData<List<Note>> noteLiveData;
    private NoteDatabase mNoteDataBase;

    public NoteViewModel(@NonNull Application application) {
        super(application);
        mNoteDataBase=NoteDatabase.getInstance(this.getApplication());
        this.noteLiveData=mNoteDataBase.getNoteDao().getAllNoteLive();
    }

    public LiveData<List<Note>> getNoteLiveData(){
        return noteLiveData;
    }

    public void deleteData(Note note){
        mNoteDataBase.getNoteDao().deleteNote(note);
    }

    public void updte_specific(int note_id, String title, String description) {
        mNoteDataBase.getNoteDao().update_specific(note_id,title,description);
    }

    public void insertNote(Note note) {
    mNoteDataBase.getNoteDao().insertNote(note);
    }
}
