package com.example.android.note_using_roomdb.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.android.note_using_roomdb.Constants;
import com.example.android.note_using_roomdb.Model.Note;
import com.example.android.note_using_roomdb.Model.NoteDao;

/**
 * Created by parth on 6/3/18.
 */
@Database(entities = { Note.class }, version = 1)
public abstract class NoteDatabase extends RoomDatabase {

    public abstract NoteDao getNoteDao();

    private static NoteDatabase noteDB;

    public static NoteDatabase getInstance(Context context){
        if(noteDB==null){
            noteDB=buildDatabaseInstance(context);
        }
        return noteDB;
    }

    private static NoteDatabase buildDatabaseInstance(Context context) {
    return Room.databaseBuilder(context,NoteDatabase.class, Constants.DB_NAME)
            .allowMainThreadQueries().build();
    }

}
