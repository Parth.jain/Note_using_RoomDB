package com.example.android.note_using_roomdb.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.note_using_roomdb.Model.Note;
import com.example.android.note_using_roomdb.R;

import java.util.List;

/**
 * Created by parth on 7/3/18.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.Holder> {

    private List<Note> list;
    private Context context;
    private LayoutInflater layoutInflater;
    private OnNoteItemClick onNoteItemClick;

    public NoteAdapter(List<Note> list,Context context) {
        layoutInflater = LayoutInflater.from(context);
        this.list = list;
        this.context = context;
        onNoteItemClick = (OnNoteItemClick) context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.note_list_item,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Log.e("bind", "onBindViewHolder: "+ list.get(position));
        holder.textViewTitle.setText(list.get(position).getTitle());
        holder.textViewContent.setText(list.get(position).getContent());
    }


    @Override
    public int getItemCount() {
        if (list != null)
            return list.size();
        else return 0;
    }
    public void setChange(List<Note> notes){
        this.list=notes;
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewContent;
        TextView textViewTitle;
        public Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textViewContent = itemView.findViewById(R.id.item_text);
            textViewTitle = itemView.findViewById(R.id.tv_title);
        }

       @Override
        public void onClick(View view) {
            Log.e("Position", String.valueOf(getAdapterPosition()));
            onNoteItemClick.onNoteClick(getAdapterPosition());
        }
    }
    public interface OnNoteItemClick{
        void onNoteClick(int pos);
    }
}
